﻿using PSMT.UpdateManager.AI.Domain;
using System;
using System.Linq;
using System.Collections.Generic;

namespace PSMT.UpdateManager.AI.MongoDbRepository
{
    public class PSAfeUserDataRepository
    {

        private PSafeUserData ToDomain(string line, long timestamp)
        {

            PSafeUserData user = new PSafeUserData(line);
          

            return user;

        }

        public void InsertPSafeUserDataList(string psafeList, long timestamp)
        {
            int size = 100000;

            IDictionary<int, IList<PSafeUserData>> userDataSplitted = new Dictionary<int, IList<PSafeUserData>>();

            string[] lines = psafeList.Split('\n');

            IList<PSafeUserData> users = new List<PSafeUserData>();

            PSafeUserData user;
            if (lines.Length <= size)
            {
                foreach (string line in lines)
                {
                    user = new PSafeUserData(line);
                    //if (!user.IsHeader)
                    users.Add(user);
                }
            }
            else 
            {
                int blockSize = (lines.Length / size);
                IEnumerable<string> blockedlines;
                for (int i = 1; i <= blockSize; i++)
                {
                    users = new List<PSafeUserData>();
                    blockedlines = lines.Take(size).ToArray();
                    foreach (string line in blockedlines)
                    {
                        user = new PSafeUserData(line);
                        //if (!user.IsHeader)
                        users.Add(user);
                    }
                    userDataSplitted.Add(i, users);
                    lines = lines.Skip(size).ToArray();
                }

            }
            
        }



    }
}
