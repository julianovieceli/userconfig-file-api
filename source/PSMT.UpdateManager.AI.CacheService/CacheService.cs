using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.CacheService
{
    public class CacheService : BackgroundService
    {
        private readonly ILogger<CacheService> _logger;
        private readonly ICacheableUpdateCFGService _cacheableUpdateCFGService;
        private readonly CacheableServiceConfiguration _cacheableServiceConfiguration;


        public CacheService(ILogger<CacheService> logger, ICacheableUpdateCFGService cacheableUpdateCFGService,
             IOptions<CacheableServiceConfiguration> cacheableServiceConfiguration)
        {
            _logger = logger;
            _cacheableUpdateCFGService = cacheableUpdateCFGService;
            _cacheableServiceConfiguration = cacheableServiceConfiguration.Value;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Cache Service started at: {time}", DateTimeOffset.Now);


                await _cacheableUpdateCFGService.UploadUpdateCFGToCache();


                _logger.LogInformation("Cache Service Finished at: {time}", DateTimeOffset.Now);

                TimeSpan ts = new TimeSpan(0, _cacheableServiceConfiguration.periodInMinutes, 0);
                await Task.Delay(ts, stoppingToken);
            }
        }
    }
}
