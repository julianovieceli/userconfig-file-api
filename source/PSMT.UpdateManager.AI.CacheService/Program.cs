using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PSMT.UpdateManager.AI.Application;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cache;
using PSMT.UpdateManager.AI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.CacheService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
             .ConfigureAppConfiguration((hostContext, configuration) =>
             {
                 string aspnetcoreEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;

                 Console.WriteLine("This is the value for 'ASPNETCORE_ENVIRONMENT': " + aspnetcoreEnv);

                 IConfigurationBuilder builder = new ConfigurationBuilder()
                     .SetBasePath(Environment.CurrentDirectory)
                     .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

                 /* Unfortunately, this is necessary once when integration tests are run, the 'env' parameter above is not set properly.
                    In other words, it is not set according what is set on ASPNETCORE_ENVIRONMENT.
                    So I made this arrangement in order to be able to run integration tests on the CI environment*/
                 if (aspnetcoreEnv.Equals("CI"))
                 {
                     builder.AddJsonFile("appsettings.CI.json", optional: true);
                 }
                 else
                 {
                     builder.AddJsonFile($"appsettings.{aspnetcoreEnv}.json", optional: true);
                 }

                 builder.AddEnvironmentVariables();
                 configuration.AddConfiguration(builder.Build());

             })
                .ConfigureServices((hostContext, services) =>
                {
                    string cacheConnString = hostContext.Configuration.GetValue<string>("cacheConnectionString");

                    string cachePassword = string.Empty;
                    if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != "Production")
                        cachePassword = hostContext.Configuration.GetValue<string>("cachePassword");

                     cacheConnString = string.Format(cacheConnString, cachePassword);

                    //Console.WriteLine($"log:{cacheConnString}");



                    services.AddStackExchangeRedisCache(options =>
                    {
                        options.Configuration = cacheConnString;
                        //options.ConfigurationOptions.Password = hostContext.Configuration.GetValue<string>("CacheableServiceConfiguration:cachePassword"); 
                    });

                    services.Configure<CacheableServiceConfiguration>(hostContext.Configuration.GetSection(nameof(CacheableServiceConfiguration)));
                    services.AddScoped<ICacheableUpdateCFGService, CacheableUpdateCFGService>();

                    services.AddScoped<ICacheService, RedisCacheService>();

                  

                    services.AddHostedService<CacheService>();
                });
    }
}
