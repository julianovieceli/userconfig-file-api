﻿namespace PSMT.UpdateManager.AI.Api.Models
{
    public class Response<D>
    {
        public int result;
        public D data;
    }
}