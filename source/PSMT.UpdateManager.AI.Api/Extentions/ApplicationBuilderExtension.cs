﻿using Microsoft.AspNetCore.Builder;
using Prometheus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Api.Extentions
{
    public static class ApplicationBuilderExtension
    {
        public static void UseCustomMetrics(this IApplicationBuilder builder)
        {
            // Custom Metrics to count requests for each endpoint and the method
            var counter = Metrics.CreateCounter("update_manager_api_total_counter", "Total Requests", new CounterConfiguration
            {
                LabelNames = new[] { "method", "endpoint", "status" }
               
            });

            builder.Use((context, next) =>
            {

                if ((context.Request.Path.Value != "/metrics") && (context.Request.Path.Value.Contains("/api/user/updatecfg")))
                {
                    var path = context.Request.Path.Value.Split("/")[3];
                    //counter.WithLabels(context.Request.Method, context.Response.StatusCode.ToString()).Inc();
                    counter.WithLabels(context.Request.Method, path, context.Response.StatusCode.ToString()).Inc();
                }
                /*if (context.Request.Path.Value.Contains("/api/user/updatecfg"))
                {
                    counter.WithLabels(context.Request.Method, "teste", context.Response.StatusCode.ToString()).Inc();
                    return next();
                };*/
                return next();
            });

        }
    }
}
