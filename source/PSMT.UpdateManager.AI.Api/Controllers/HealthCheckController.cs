﻿using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Api.Controllers
{
    [Route("/health")]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {
       

        public HealthCheckController()
        {
            
        }

        /// <summary>
        /// Just a ahealth check of the endpoint of the application
        /// </summary>
        /// <returns></returns>
        [HttpGet("live")]
        public IActionResult Live()
        {
            return Ok("Healthcheck from UpdateManagerAI Api live successfull");
        }

        //Put a service method who goes to database...note ready yet
        [HttpGet("ready")]
        public IActionResult Ready()
        {
            return Ok("Healthcheck from UpdateManagerAI Api ready successfull");
        }
    }
}