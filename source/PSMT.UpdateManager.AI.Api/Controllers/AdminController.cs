﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PSMT.UpdateManager.AI.Contracts;
using System.Text.Json;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Api.Controllers
{
    [Route("api/[controller]")]
   
    [ApiController]
    public class AdminController : ControllerBase
    {
        private IProviderUpdateCFGService _providerUpdateCFGService;


        public AdminController(IProviderUpdateCFGService providerUpdateCFGService)
        {
            _providerUpdateCFGService = providerUpdateCFGService;
        }


        /// <summary>
        /// Request made by UpdataManager UI so it enables or disables the IA Api throw the "Panic Button"
        /// </summary>
        /// <param name="panic"></param>
        /// <returns></returns>
        [EnableCors("CorsPolicy")]
        [HttpPost("panic")]
        public async Task<IActionResult> ActivatePanicButton([FromBody] JsonElement panic)
        {
            async Task<bool> ActivatePanicButtonTransaction()
            {
                // return await _providerUpdateCFGService.ActivatePanicButton(Convert.ToInt32(panic["productId"]), Convert.ToBoolean(panic["active"]));
                return await _providerUpdateCFGService.ActivatePanicButton(panic.GetProperty("productId").GetInt32(), panic.GetProperty("active").GetBoolean());
            }

            return await this.DoBusinessTransactionAsync(ActivatePanicButtonTransaction, x => 0, () => 1, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product_id"></param>
        /// <returns></returns>
        [EnableCors("CorsPolicy")]
        [HttpGet("panic/{product_id}")]
        public async Task<IActionResult> GetPanicButton(int product_id)
        {
            async Task<bool> GetPanicButtonTransaction()
            {
                return await _providerUpdateCFGService.GetPanicButton(product_id);
            }

            return await this.DoBusinessTransactionAsync(GetPanicButtonTransaction, x => 0, () => 1, true);
        }



    }
}
