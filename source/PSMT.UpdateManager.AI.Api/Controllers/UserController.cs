﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PSMT.UpdateManager.AI.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Api.Controllers
{
    [Route("api/[controller]")]
    //[EnableCors("SiteCorsPolicy")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IProviderUpdateCFGService _providerUpdateCFGService;


        public UserController(IProviderUpdateCFGService providerUpdateCFGService)
        {
            _providerUpdateCFGService = providerUpdateCFGService;
        }


        /// <summary>
        /// Gets the update.cfg from an user. NOT ecrypted. For admins to see the file...
        /// </summary>
        /// <param name="product_id">The Id of the product</param>
        /// <param name="psafe_id">The psafeId of the user</param>
        /// <returns></returns>
        [HttpGet("updatecfg_internal/{product_id}/{psafe_id}")]
        public async Task<IActionResult> GetUpdateCFGFileAsync(int product_id, string psafe_id)
        {
            async Task<UpdateCFG> GetUpdateCFGFileTransaction()
            {
                return await _providerUpdateCFGService.GetUpdateCFGFileAsync(product_id, psafe_id);
            }

            return await this.DoBusinessTransactionAsync(GetUpdateCFGFileTransaction, x => 0, () => 1, true);
        }

        /// <summary>
        /// Gets the update.cfg signed and encripted from an user.
        /// </summary>
        /// <param name="product_id">The Id of the product</param>
        /// <param name="psafe_id">The psafeId of the user</param>
        /// <returns></returns>
        [HttpGet("updatecfg/{product_id}/{psafe_id}")]
        public async Task<IActionResult> GetUpdateCFGFileEncryptedAsync(int product_id, string psafe_id)
        {
            async Task<byte[]> GetUpdateCFGFileEncryptedTransaction()
            {
                return await _providerUpdateCFGService.GetUpdateCFGFileEncryptedAsync(product_id, psafe_id);
            }


            IActionResult result = await this.DoBusinessTransactionAsync(GetUpdateCFGFileEncryptedTransaction, x =>  0, () => 1, true);

            if(result is ConflictObjectResult)
            {
                return (result);
            }
            else
                return File(((OkObjectResult)result).Value as byte[], System.Net.Mime.MediaTypeNames.Application.Octet, "response.ai.cfg");

        }

    }
}
