﻿using Microsoft.AspNetCore.Mvc;
using PSMT.UpdateManager.AI.Api.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Api.Controllers
{
    public static class ControllerBaseExtensions
    {
        /// <summary>
        /// Gives you the chance to execute your Business Transaction handling all the 'boilerplate' code regarding
        /// exception handling, response creation, error logging and so on.
        /// </summary>
        /// <param name="DoTransation">Your business method</param>
        /// <param name="GetClientResult">Let you return the 'result' code that should be delivered to the client </param>
        /// <param name="GetExceptionClientResult">Let you define the 'result' code to be returned to the client in case of an application error</param>
        /// <param name="appendDataOnResponse">If true, the returned data from your business method will be included in the client response</param>
        /// <returns></returns>
        public static IActionResult DoBusinessTransaction(this ControllerBase controllerBase,
                                                               Action DoTransation,
                                                               Func<int> GetClientResult,
                                                               Func<int> GetExceptionClientResult,
                                                               bool appendDataOnResponse)
        {
            /* This may sound a little bit weird, but it has a purpose: the overloading versiong of this method that implements all the logic,
             * expects a Func<TReturned> delegate, so it would be necessary to provide a method that returns something.
             * As the purpose of this overloading is to provide a returnless method, the bellow local methods are created as dummy boolean return methods
             * so we can take advantage of the other overlading where the logic is implemented */
            bool ActionWrapper()
            {
                DoTransation();
                return true;
            }

            int GetClientWrapper(bool dummy)
            {
                return GetClientResult();
            }

            return DoBusinessTransaction<bool>(controllerBase, ActionWrapper, GetClientWrapper, GetExceptionClientResult, appendDataOnResponse);
        }

        /// <summary>
        /// Gives you the chance to execute your Business Transaction handling all the 'boilerplate' code regarding
        /// exception handling, response creation, error logging and so on.
        /// </summary>
        /// <typeparam name="TReturned">The returned type of your business method</typeparam>
        /// <param name="DoTransation">Your business method</param>
        /// <param name="GetClientResult">Gives you the returning of your business method and let you return the 'result' code that should be delivered to the client </param>
        /// <param name="GetExceptionClientResult">Let you define the 'result' code to be returned to the client in case of an application error</param>
        /// <param name="appendDataOnResponse">If true, the returned data from your business method will be included in the client response</param>
        /// <returns></returns>
        public static IActionResult DoBusinessTransaction<TReturned>(this ControllerBase controllerBase,
                                                               Func<TReturned> DoTransation,
                                                               Func<TReturned, int> GetClientResult,
                                                               Func<int> GetExceptionClientResult,
                                                               bool appendDataOnResponse)
        {
            try
            {
                TReturned data = DoTransation();

                if (appendDataOnResponse)
                {
                    Response<TReturned> appResponse = new Response<TReturned>();

                    appResponse.result = GetClientResult(data);
                    appResponse.data = data;

                    return controllerBase.Ok(appResponse);
                }
                else
                {
                    Response<string> appResponse = new Response<string>();
                    appResponse.result = GetClientResult(data);

                    return controllerBase.Ok(appResponse);
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is ArgumentNullException || ex is ApplicationException)
                {
                    Response<string> applicationErrorResponse = new Response<string>();

                    applicationErrorResponse.result = GetExceptionClientResult();
                    applicationErrorResponse.data = ex.Message;

                    string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                    if (env == null)
                        env = string.Empty;

                    if (!env.Equals("Production"))
                    {
                        Console.WriteLine("");
                        Console.WriteLine("*** Client Conflit request ****");
                        Console.WriteLine(ex.ToString());
                        Console.WriteLine("");
                    }

                    return controllerBase.Conflict(applicationErrorResponse);
                }
                else
                {
                    //The applicatino container is set to get everything that is sent to "stdout" and send to Elastic Search / Kibana.
                    //Application don't need to do nothing... only throw! :)
                    throw;
                }
            }
        }

        /// <summary>
        /// Gives you the chance to execute your Business Transaction handling all the 'boilerplate' code regarding
        /// exception handling, response creation, error logging and so on.
        /// </summary>
        /// <param name="DoTransaction">Your business method</param>
        /// <param name="GetClientResult">Let you return the 'result' code that should be delivered to the client </param>
        /// <param name="GetExceptionClientResult">Let you define the 'result' code to be returned to the client in case of an application error</param>
        /// <param name="appendDataOnResponse">If true, the returned data from your business method will be included in the client response</param>
        /// <returns></returns>
        public async static Task<IActionResult> DoBusinessTransactionAsync(this ControllerBase controllerBase,
                                                               Func<Task> DoTransaction,
                                                               Func<int> GetClientResult,
                                                               Func<int> GetExceptionClientResult,
                                                               bool appendDataOnResponse)
        {
            /* This may sound a little bit weird, but it has a purpose: the overloading version of this method that implements all the logic,
             * expects a Func<TReturned> delegate, so it would be necessary to provide a method that returns something.
             * As the purpose of this overloading is to provide a returnless method, the bellow local methods are created as dummy boolean return methods
             * so we can take advantage of the other overlading where the logic is implemented */
            async Task<bool> ActionWrapper()
            {
                await DoTransaction();
                return true;
            }

            int GetClientWrapper(bool dummy)
            {
                return GetClientResult();
            }

            return await DoBusinessTransactionAsync<bool>(controllerBase, ActionWrapper, GetClientWrapper, GetExceptionClientResult, appendDataOnResponse);
        }

        /// <summary>
        /// Gives you the chance to asynchronously execute your Business Transaction handling all the 'boilerplate' code regarding
        /// exception handling, response creation, error logging and so on.
        /// </summary>
        /// <typeparam name="TReturned">The returned type of your business method</typeparam>
        /// <param name="DoTransaction">Your business method</param>
        /// <param name="GetClientResult">Gives you the returning of your business method and let you return the 'result' code that should be delivered to the client </param>
        /// <param name="GetExceptionClientResult">Let you define the 'result' code to be returned to the client in case of an application error</param>
        /// <param name="appendDataOnResponse">If true, the returned data from your business method will be included in the client response</param>
        /// <returns></returns>
        public static async Task<IActionResult> DoBusinessTransactionAsync<TReturned>(this ControllerBase controllerBase,
                                                               Func<Task<TReturned>> DoTransaction,
                                                               Func<TReturned, int> GetClientResult,
                                                               Func<int> GetExceptionClientResult,
                                                               bool appendDataOnResponse
                                                             
                                                                )
        {
            try
            {
                TReturned data = await DoTransaction();

                if (appendDataOnResponse)
                {
                  
                        return controllerBase.Ok(data);
              
                }
                else
                {
                    GetClientResult(data);

                    return controllerBase.Ok();
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is ArgumentNullException || ex is ApplicationException)
                {
                    ResponseException applicationErrorResponse = new ResponseException();

                    applicationErrorResponse.message = ex.Message;
                    applicationErrorResponse.errorCode = (int)HttpStatusCode.Conflict;

                    string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                    if (env == null)
                        env = string.Empty;

                    if (!env.Equals("Production"))
                    {
                        Console.WriteLine("");
                        Console.WriteLine("*** Client Conflit request ****");
                        Console.WriteLine(ex.ToString());
                        Console.WriteLine("");
                    }

                    return controllerBase.Conflict(new { message = ex.Message }); 
                }
                else
                {
                    // The application container is set to get everything that is sent to "stdout" and send to Elastic Search / Kibana.
                    // Application doesn't need to do anything... only throw! :)
                    throw;
                }
            }
        }

     
    }
}