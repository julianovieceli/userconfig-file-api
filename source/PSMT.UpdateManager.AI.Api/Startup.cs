using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PSMT.UpdateManager.AI.Application;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cache;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using PSMT.UpdateManager.AI.Infrastructure;
using System;
using System.Configuration;
using System.Reflection;
using System.IO;
using Prometheus;
using PSMT.UpdateManager.AI.Api.Extentions;

namespace PSMT.UpdateManager.AI.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env)
        {

            string aspnetcoreEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;


            Console.WriteLine("This is the value for 'ASPNETCORE_ENVIRONMENT': " + aspnetcoreEnv);
            Console.WriteLine("This is my env: " + env.EnvironmentName);

            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            /* Unfortunately, this is necessary once when integration tests are run, the 'env' parameter above is not set properly. 
               In other words, it is not set according what is set on ASPNETCORE_ENVIRONMENT.
               So I made this arrangement in order to be able to run integration tests on the CI environment*/
            if (aspnetcoreEnv.Equals("CI"))
            {
                builder.AddJsonFile("appsettings.CI.json", optional: true);
            }
            else
            {
                builder.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();


         
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                       
                    });
            });

            services.AddControllers();


            services.AddInfrastrucutureServices();

            services.AddScoped<IProviderUpdateCFGService, ProviderUpdateCFGService>();
            services.Configure<CloudServiceSettings>(Configuration.GetSection(nameof(CloudServiceSettings)));

            string cacheConnString = Configuration.GetValue<string>("cacheConnectionString");
            string cachePassword = string.Empty;
            if(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != "Production")
                cachePassword = Configuration.GetValue<string>("cachePassword");

            cacheConnString = string.Format(cacheConnString, cachePassword);

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = cacheConnString;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "UpdateManager AI.Api", Version = "v1" , Description = "API to provide update.cfg file from IA for an user/product",
                    Contact = new OpenApiContact
                    {
                        Name = "Juliano Vieceli",
                        Email = "juliano.vieceli@psafe.com"
                       
                    }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                Console.WriteLine(xmlPath);
                c.IncludeXmlComments(xmlPath);
            });

            


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

           
            // Use the Prometheus middleware
            app.UseCustomMetrics();
            app.UseMetricServer();
            app.UseHttpMetrics();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            if (env.IsDevelopment() || env.IsStaging())
            {
                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
                // specifying the Swagger JSON endpoint.
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "UpdateManager.AI.Api v1");
                    //c.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();


            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
                endpoints.MapControllers();
       
            });

           
            

     
        }
    }
}
