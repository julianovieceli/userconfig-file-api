﻿using Moq;
using PSMT.UpdateManager.AI.Contracts.Cache;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.UnitTests.Mock
{
    public static class CacheServiceMock
    {
        public static ICacheService Get(string key, CachedData cachedData)
        {
            var mockCacheService = new Mock<ICacheService>();
            mockCacheService.Setup(m => m.GetDataAsync(key)).Returns(Task.FromResult(cachedData)); ;
            ICacheService cacheService = mockCacheService.Object;

            return cacheService;
        }
    }
}
