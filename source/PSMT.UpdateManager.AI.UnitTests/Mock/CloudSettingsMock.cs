﻿using Microsoft.Extensions.Options;
using Moq;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.UnitTests.Mock
{
    public static class CloudSettingsMock
    {
        public static IOptions<CloudServiceSettings> Get()
        {
            CloudServiceSettings cloudServiceSettings = new CloudServiceSettings();
            cloudServiceSettings.key = "somePath";

            var mockCloudServiceSettings = new Mock<IOptions<CloudServiceSettings>>();
            mockCloudServiceSettings.Setup(m => m.Value).Returns(cloudServiceSettings);
            IOptions<CloudServiceSettings> cloudServiceSettingsOptions = mockCloudServiceSettings.Object;

            return cloudServiceSettingsOptions;
        }
    }
}
