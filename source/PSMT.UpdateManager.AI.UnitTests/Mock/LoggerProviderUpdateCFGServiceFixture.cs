﻿using Microsoft.Extensions.Logging;
using Moq;
using PSMT.UpdateManager.AI.Application;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace PSMT.UpdateManager.AI.UnitTests.Mock
{
    public class LoggerProviderUpdateCFGServiceFixture : IDisposable
    {

        private ILogger<ProviderUpdateCFGService> _logger;

        public LoggerProviderUpdateCFGServiceFixture()
        {
            Mock<ILogger<ProviderUpdateCFGService>> mockLogger = new Mock<ILogger<ProviderUpdateCFGService>>();
            _logger = mockLogger.Object;
        }

        public void Dispose()
        {

        }


        public ILogger<ProviderUpdateCFGService> Logger { get { return _logger; } }
    }

    /*[CollectionDefinition("Logger collection")]
    public class LoggerCollection : ICollectionFixture<LoggerProviderUpdateCFGServiceFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }*/
}
