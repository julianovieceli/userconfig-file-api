﻿using Moq;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.UnitTests.Mock
{
    public static class CloudServiceMock
    {
        public static ICloudService Get(ResponseCloudDto responseCloudDto, string psafeId)
        {
          
            var mockCloudService = new Mock<ICloudService>();
            //mockCloudService.Setup(m => m.DownloadUserCSVFileByHttpRequestAsync("somePath", responseCloudDto.ProductId, responseCloudDto.ProductName, psafeId)).Returns(Task.FromResult(responseCloudDto));
            mockCloudService.Setup(m => m.DownloadUserCSVFileByClientSDKAsync("somePath", responseCloudDto.ProductId, responseCloudDto.ProductName, psafeId)).Returns(Task.FromResult(responseCloudDto));
            ICloudService cloudService = mockCloudService.Object;

            return cloudService;
        }
    }
}
