using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using PSMT.UpdateManager.AI.Application;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cache;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using PSMT.UpdateManager.AI.UnitTests.Mock;
using System;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace PSMT.UpdateManager.AI.UnitTests
{

    //[CollectionDefinition("Logger collection")]
    public class ProviderUpdateCFGServiceTests : IClassFixture<LoggerProviderUpdateCFGServiceFixture>
    {

        private LoggerProviderUpdateCFGServiceFixture _logger;

        private IOptions<CloudServiceSettings> _cloudServiceSettings;

        public ProviderUpdateCFGServiceTests(LoggerProviderUpdateCFGServiceFixture fixture)
        {
            _logger = fixture;
            _cloudServiceSettings = CloudSettingsMock.Get();
        }


        [Theory]
        [InlineData(1, "", "error")]
        [InlineData(0, "123", "Invalid")]
        public async Task GetUpdateCFGFileRequiredDataAsyncError(int productId, string psafeId, string errorMessage)
        {

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(null,
            _logger.Logger, _cloudServiceSettings, null);

            var exception = await Assert.ThrowsAsync<ApplicationException>(() => providerService.GetUpdateCFGFileAsync(productId, psafeId));

            Assert.Contains(errorMessage, exception.Message);
        }

        [Fact]
        public async Task GetUpdateCFGFileAsyncNoCacheError()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = string.Empty;

        
            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(null,
            _logger.Logger, _cloudServiceSettings, CacheServiceMock.Get(keyFromCache, null));

            var exception = await Assert.ThrowsAsync<ApplicationException>(() => providerService.GetUpdateCFGFileAsync(productId, psafeId));

            Assert.Contains("error", exception.Message);
        }


        [Fact]
        public async Task GetUpdateCFGFileAsyncInactiveProductError()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            CachedData cachedData = new CachedData(productId, productName, false, null);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(null,
            _logger.Logger, _cloudServiceSettings, cacheService);

            var exception = await Assert.ThrowsAsync<ApplicationException>(() => providerService.GetUpdateCFGFileAsync(productId, psafeId));

            Assert.Contains("error", exception.Message);
        }

        [Fact]
        public async Task GetUpdateCFGFileAsyncResponseFromCloudError()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            responseCloudDto.StatusDownloadFile = StatusDownloadFile.Error;
            responseCloudDto.Message = "Error in download";


            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            CachedData cachedData = new CachedData(productId, productName, true, null);


            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            var exception = await Assert.ThrowsAsync<ApplicationException>(() => providerService.GetUpdateCFGFileAsync(productId, psafeId));

            Assert.Contains("error", exception.Message);

        }

        [Fact]
        public async Task GetUpdateCFGFileAsyncContentError()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            responseCloudDto.StatusDownloadFile = StatusDownloadFile.Success;
        

            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            UpdateCFG updateCFG = new UpdateCFG();
            CachedData cachedData = new CachedData(productId, productName, true, updateCFG);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            var exception = await Assert.ThrowsAsync<ApplicationException>(() => providerService.GetUpdateCFGFileAsync(productId, psafeId));

            Assert.Contains("User data in .csv file. PSAfeId", exception.Message);
            

        }

        [Fact]
        public async Task GetUpdateCFGFileAsyncUserDataContentError()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            responseCloudDto.FileCSVDownloadedDto.Content = "Some bullshit";
            responseCloudDto.StatusDownloadFile = StatusDownloadFile.Success;


            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            UpdateCFG updateCFG = new UpdateCFG();
            CachedData cachedData = new CachedData(productId, productName, true, updateCFG);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            var exception = await Assert.ThrowsAsync<ApplicationException>(() => providerService.GetUpdateCFGFileAsync(productId, psafeId));

            Assert.Contains("User data in .csv file. PSAfeId", exception.Message);


        }


        [Fact]
        public async Task GetUpdateCFGFileAsyncUserDataSuccess()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            //The perfect content...           
            string versionCodeRange = "73000";
            string timestamp = "1626387688";
            responseCloudDto.FileCSVDownloadedDto.Content = $"00b9bee25bfbf8c31e148adaa5608000,{versionCodeRange},http://up2date.mob.psafe.com/psafe/updateData/1111/notification_lpt.cfg,252bcff712003984e9fbe7fdbc8e6f71,http://up2date.mob.psafe.com/psafe/updateData/2222/notification_les.cfg,252bcff712003984e9fbe7fdbc8e6f72,http://up2date.mob.psafe.com/psafe/updateData/3333/notification_len.cfg,252bcff712003984e9fbe7fdbc8e6f73,http://up2date.mob.psafe.com/psafe/updateData/4444/cardlist_lpt.cfg,252bcff712003984e9fbe7fdbc8e6f71,http://up2date.mob.psafe.com/psafe/updateData/5555/cardlist_les.cfg,252bcff712003984e9fbe7fdbc8e6f72,http://up2date.mob.psafe.com/psafe/updateData/6666/cardlist_len.cfg,252bcff712003984e9fbe7fdbc8e6f73,http://up2date.mob.psafe.com/psafe/updateData/7777/dialog_lpt.cfg,252bcff712003984e9fbe7fdbc8e6f71,http://up2date.mob.psafe.com/psafe/updateData/8888/dialog_les.cfg,252bcff712003984e9fbe7fdbc8e6f72,http://up2date.mob.psafe.com/psafe/updateData/9999/dialog_len.cfg,252bcff712003984e9fbe7fdbc8e6f73,{timestamp}";
            responseCloudDto.StatusDownloadFile = StatusDownloadFile.Success;


            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            UpdateCFG updateCFG = new UpdateCFG();

            updateCFG.files = new System.Collections.Generic.List<File>();
            File file = new File();
            file.file = "notification_les.cfg";
            updateCFG.files.Add(file);

            file = new File();
            file.file = "cardlist_len.cfg";
            updateCFG.files.Add(file);

            file = new File();
            file.file = "dialog_les.cfg";
            updateCFG.files.Add(file);

            CachedData cachedData = new CachedData(productId, productName, true, updateCFG);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            UpdateCFG updateCFGMerged = await providerService.GetUpdateCFGFileAsync(productId, psafeId);

            Assert.True(updateCFGMerged != null);


            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("1111")?1:0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("2222") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("3333") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("4444") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("5555") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("6666") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("7777") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("8888") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("9999") ? 1 : 0)));


            Assert.Equal(versionCodeRange, Convert.ToString(updateCFGMerged.files[0].filter.versionCodeRange.first));


            Assert.Equal(timestamp, Convert.ToString(updateCFGMerged.files[0].timestamp));


        }


        [Fact]
        public async Task GetUpdateCFGFileAsyncUserOnyNotificationDataSuccess()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            //The perfect content...           
            string versionCodeRange = "73000";
            string timestamp = "1626387688";
            responseCloudDto.FileCSVDownloadedDto.Content = $"00b9bee25bfbf8c31e148adaa5608000,{versionCodeRange},http://up2date.mob.psafe.com/psafe/updateData/1111/notification_lpt.cfg,252bcff712003984e9fbe7fdbc8e6f71,http://up2date.mob.psafe.com/psafe/updateData/2222/notification_les.cfg,252bcff712003984e9fbe7fdbc8e6f72,http://up2date.mob.psafe.com/psafe/updateData/3333/notification_len.cfg,252bcff712003984e9fbe7fdbc8e6f73,,,,,,,,,,,,,{timestamp}";
            responseCloudDto.StatusDownloadFile = StatusDownloadFile.Success;


            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            UpdateCFG updateCFG = new UpdateCFG();

            updateCFG.files = new System.Collections.Generic.List<File>();
            File file = new File();
            file.file = "notification_les.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            file = new File();
            file.file = "cardlist_len.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            file = new File();
            file.file = "dialog_les.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            CachedData cachedData = new CachedData(productId, productName, true, updateCFG);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            UpdateCFG updateCFGMerged = await providerService.GetUpdateCFGFileAsync(productId, psafeId);

            Assert.True(updateCFGMerged != null);


            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("1111") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("2222") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("3333") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("4444") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("5555") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("6666") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("7777") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("8888") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("9999") ? 1 : 0)));

            File firstNotificationFile = updateCFGMerged.files.First(f => f.file.Contains("notification_l"));

            Assert.Equal(versionCodeRange, Convert.ToString(firstNotificationFile.filter. versionCodeRange.first));


            Assert.Equal(timestamp, Convert.ToString(firstNotificationFile.timestamp));


        }


        [Fact]
        public async Task GetUpdateCFGFileAsyncUserOnyCardListDataSuccess()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            //The perfect content...           
            string versionCodeRange = "73000";
            string timestamp = "1626387688";
               responseCloudDto.FileCSVDownloadedDto.Content = $"00b9bee25bfbf8c31e148adaa5608000,{versionCodeRange},,,,,,,http://up2date.mob.psafe.com/psafe/updateData/4444/cardlist_lpt.cfg,252bcff712003984e9fbe7fdbc8e6f71,http://up2date.mob.psafe.com/psafe/updateData/5555/cardlist_les.cfg,252bcff712003984e9fbe7fdbc8e6f72,http://up2date.mob.psafe.com/psafe/updateData/6666/cardlist_len.cfg,252bcff712003984e9fbe7fdbc8e6f73,,,,,,,{timestamp}";
         responseCloudDto.StatusDownloadFile = StatusDownloadFile.Success;


            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            UpdateCFG updateCFG = new UpdateCFG();

            updateCFG.files = new System.Collections.Generic.List<File>();
            File file = new File();
            file.file = "notification_les.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            file = new File();
            file.file = "cardlist_len.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            file = new File();
            file.file = "dialog_les.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            CachedData cachedData = new CachedData(productId, productName, true, updateCFG);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            UpdateCFG updateCFGMerged = await providerService.GetUpdateCFGFileAsync(productId, psafeId);

            Assert.True(updateCFGMerged != null);


            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("1111") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("2222") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("3333") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("4444") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("5555") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("6666") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("7777") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("8888") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("9999") ? 1 : 0)));

            File firstCardListFile = updateCFGMerged.files.First(f => f.file.Contains("cardlist_l"));

            Assert.Equal(versionCodeRange, Convert.ToString(firstCardListFile.filter.versionCodeRange.first));


            Assert.Equal(timestamp, Convert.ToString(firstCardListFile.timestamp));


        }

        [Fact]
        public async Task GetUpdateCFGFileAsyncUserOnyDialogDataSuccess()
        {
            int productId = 1;
            string productName = "DFNDR Security";
            string psafeId = Guid.NewGuid().ToString();
            string keyFromCache = "dfndr_security";

            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);
            //The perfect content...           
            string versionCodeRange = "73000";
            string timestamp = "1626387688";
            responseCloudDto.FileCSVDownloadedDto.Content = $"00b9bee25bfbf8c31e148adaa5608000,{versionCodeRange},,,,,,,,,,,,,http://up2date.mob.psafe.com/psafe/updateData/7777/dialog_lpt.cfg,252bcff712003984e9fbe7fdbc8e6f71,http://up2date.mob.psafe.com/psafe/updateData/8888/dialog_les.cfg,252bcff712003984e9fbe7fdbc8e6f72,http://up2date.mob.psafe.com/psafe/updateData/9999/dialog_len.cfg,252bcff712003984e9fbe7fdbc8e6f73,{timestamp}";
            responseCloudDto.StatusDownloadFile = StatusDownloadFile.Success;


            ICloudService cloudService = CloudServiceMock.Get(responseCloudDto, psafeId);

            UpdateCFG updateCFG = new UpdateCFG();

            updateCFG.files = new System.Collections.Generic.List<File>();
            File file = new File();
            file.file = "notification_les.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            file = new File();
            file.file = "cardlist_len.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            file = new File();
            file.file = "dialog_les.cfg";
            file.serverFile = "somePath/" + file.file;
            updateCFG.files.Add(file);

            CachedData cachedData = new CachedData(productId, productName, true, updateCFG);

            ICacheService cacheService = CacheServiceMock.Get(keyFromCache, cachedData);

            IProviderUpdateCFGService providerService = new ProviderUpdateCFGService(cloudService,
            _logger.Logger, _cloudServiceSettings, cacheService);

            UpdateCFG updateCFGMerged = await providerService.GetUpdateCFGFileAsync(productId, psafeId);

            Assert.True(updateCFGMerged != null);


            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("1111") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("2222") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("3333") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("4444") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("5555") ? 1 : 0)));
            Assert.Equal(0, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("6666") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("7777") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("8888") ? 1 : 0)));
            Assert.Equal(1, updateCFGMerged.files.Sum(f => (f.serverFile.Contains("9999") ? 1 : 0)));


            File firstDialogFile = updateCFGMerged.files.First(f => f.file.Contains("dialog_l"));

            Assert.Equal(versionCodeRange, Convert.ToString(firstDialogFile.filter.versionCodeRange.first));


            Assert.Equal(timestamp, Convert.ToString(firstDialogFile.timestamp));


        }
    }

       
}
