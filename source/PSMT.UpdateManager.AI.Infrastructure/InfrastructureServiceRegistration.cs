﻿using Microsoft.Extensions.DependencyInjection;
using PSMT.UpdateManager.AI.Contracts.Cache;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Infrastructure
{
    public static class InfrastructureServiceRegistration
    {
        public static IServiceCollection AddInfrastrucutureServices(this IServiceCollection services)
        {
            services.AddScoped<ICloudService, AWSService>();
            services.AddScoped<ICacheService, RedisCacheService>();

            return services;
        }
    }
}
