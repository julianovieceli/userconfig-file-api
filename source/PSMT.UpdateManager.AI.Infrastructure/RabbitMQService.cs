﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts.Queue;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Infrastructure
{
    public class RabbitMQService : IQueueService
    {
        private IConnection _conn;
        private readonly ILogger<RabbitMQService> _logger;
        private readonly QueueServiceSettings _queueServiceSettings;
        private ConnectionFactory _factory;

        public RabbitMQService(ILogger<RabbitMQService> logger, IOptions<QueueServiceSettings> queueServiceSettings)
        {
            _logger = logger;

            //ConnectionFactory factory = new ConnectionFactory();
            //factory.Uri = new Uri("amqp://teste:teste@updateManagerAIHost:8080/vhost");
            _queueServiceSettings = queueServiceSettings.Value;
            _factory = new ConnectionFactory() { HostName = _queueServiceSettings.localhost, UserName = _queueServiceSettings.user, Password = _queueServiceSettings.password, VirtualHost = "/",  Port = _queueServiceSettings.port };

            _factory.AutomaticRecoveryEnabled = true;
            _factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(60);
            //ConnectionFactory factory.Uri = new Uri("amqp://guest:guest@hostName:5672/vhost");
        }

        public void PublishMessage(string queueName, string messageList)
        {
            try
            {
                _conn = _factory.CreateConnection();


                using (var channel = _conn.CreateModel())
                {
                    channel.QueueDeclare(queue: queueName,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    foreach (string message in messageList.Split('\n'))
                    {
                        var body = Encoding.UTF8.GetBytes(message);

                        var properties = channel.CreateBasicProperties();
                        properties.Persistent = true;
                        properties.DeliveryMode = 2;//persistent
                        properties.ContentType = "text/plain";
                        
                        channel.BasicPublish(exchange: "",
                                             routingKey: queueName,
                                             basicProperties: null,
                                             body: body);
                    }


                }
            }
            catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException e)
            {
                //Thread.Sleep(5000);
                // apply retry logic
                _logger.LogError("Error in connection RabbitMQ. Exception '{0}'", e.Message);
            }
            finally
            {
                _conn.Close();
            }
        }
    }
}
