﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cache;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Infrastructure
{
    public class RedisCacheService: ICacheService
    {
        private readonly IDistributedCache _cache;

        private readonly ILogger<RedisCacheService> _logger;

        public RedisCacheService(IDistributedCache cache, ILogger<RedisCacheService> logger)
        {
            _cache = cache;
            _logger = logger;
        }

        public async Task AddDataAsync(string key, string data)
        {
            try
            {
                await _cache.SetStringAsync(key, data);
            }
            catch (Exception e)
            {
                throw new ApplicationException($"Error to set cached data. Exception: {e.Message}");
            }

        }

        public async Task<CachedData> GetDataAsync(string key)
        {
            CachedData cachedData = null;


            try
            {
                var json = await _cache.GetStringAsync(key);
                if (json != null)
                {
                    cachedData = JsonSerializer.Deserialize<CachedData>(json);
                 
                }
                return cachedData;
            }
            catch(Exception e )
            {
                throw new ApplicationException($"Error from get cached data. Exception: {e.Message}");
            }
            

        }

        public async Task RemoveDataAsync(string key)
        {
            await _cache.RemoveAsync(key);
        }
    }
}
