﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts.Cloud;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Infrastructure
{
    public class AWSService : ICloudService
    {
        private readonly ILogger<AWSService> _logger;
        private readonly CloudServiceSettings _cloudServiceSettings;


        private AmazonS3Client CreateAWSClient(AmazonS3Config config)
        {
            string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (env == "Development")
            {
                var credentials = new BasicAWSCredentials(_cloudServiceSettings.accessKey, _cloudServiceSettings.secretKey);

                return new AmazonS3Client(credentials, config);
            }
            else
                return new AmazonS3Client(config);
        }

        public AWSService(ILogger<AWSService> logger, IOptions<CloudServiceSettings> cloudServiceSettings)
        {
            _logger = logger;
            _cloudServiceSettings = cloudServiceSettings.Value; 
        }
        public async Task<ResponseCloudDto> DownloadCompleteCSVFileAsync(string productPath, long productId, string productName)
        {
            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName);

            _logger.LogInformation(string.Format("Starting Download file: '{0}' of Product: '{1}' at {2}", productPath, productName, DateTime.Now));
            try
            {

                if (File.Exists(responseCloudDto.FileName))
                {
                    File.Delete(responseCloudDto.FileName);
                }

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync(productPath, HttpCompletionOption.ResponseHeadersRead);
                    if (response.IsSuccessStatusCode)
                    {
                        //long? totalValue = response.Content.Headers.ContentLength;
                        //var totalValueAsString = response.Content.Headers.FirstOrDefault(h => h.Key.Equals("Content-Length"));
                        using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync())
                        {
                            var f = new FileStream(responseCloudDto.FileName, FileMode.CreateNew);
                            await streamToReadFrom.CopyToAsync(f);
                            f.Close();
                        }

                        _logger.LogInformation(string.Format("Finished Download file: '{0}' of Product: '{1}' at {2}", productPath, productName, DateTime.Now));

                    }
                    else
                    {
                        responseCloudDto.StatusDownloadFile = StatusDownloadFile.Error;
                        responseCloudDto.Message = "Error Download product " + productPath;

                    }
                }
            }
            catch (Exception e)
            {
                responseCloudDto.StatusDownloadFile = StatusDownloadFile.Error;
                responseCloudDto.Message = "Erro Executando task " + e.Message;
            }
            return responseCloudDto;
        }

        public async Task<ResponseCloudDto> DownloadUserCSVFileByHttpRequestAsync(string userPath, long productId, string productName, string psafeID)
        {
            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName, psafeID);

            try
            {

               
                using (HttpClient client = new HttpClient())
                {
                     HttpResponseMessage response = await client.GetAsync(userPath);
                     if (response.IsSuccessStatusCode)
                    {
                         using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync())
                        {
                            using (StreamReader sr = new StreamReader(streamToReadFrom))
                            {
                                responseCloudDto.FileCSVDownloadedDto.Content = sr.ReadLine();
                                
                            }
                        }

                        responseCloudDto.StatusCode = (int)HttpStatusCode.OK;

                    }
                    else
                    {
                         responseCloudDto.StatusDownloadFile = StatusDownloadFile.Error;
                        responseCloudDto.StatusCode = (int)response.StatusCode;
                        responseCloudDto.Message = "S3 error";
                        _logger.LogError(string.Format("Error in download S3 file: PSafeId:'{0}', Product:'{1}', Error Code:'{2}'", psafeID, productName, response.StatusCode));


                    }
                }
            }
            catch (Exception e)
            {
                   responseCloudDto.StatusDownloadFile = StatusDownloadFile.Error;
                responseCloudDto.Message = "Error downloading file in S3 bucket " + e.Message;
                _logger.LogError(string.Format("Error in download S3 file: PSafeId:'{0}', Product:'{1}', Exception:'{2}'", psafeID, productName, e.Message));
            }
            return responseCloudDto;
        }

        public async Task<ResponseCloudDto> DownloadUserCSVFileByClientSDKAsync(string userPath, long productId, string productName, string psafeID)
        {
            ResponseCloudDto responseCloudDto = new ResponseCloudDto(productId, productName, psafeID);

            try
            {
                var config = new AmazonS3Config
                {
                    RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(_cloudServiceSettings.region)

                };

                using (var s3Client = this.CreateAWSClient(config))
                {
                    GetObjectRequest request = new GetObjectRequest();
                    request.BucketName = _cloudServiceSettings.bucketName;
                    request.Key = userPath;


                    using (var response = await s3Client.GetObjectAsync(request))
                    using (Stream responseStream = response.ResponseStream)
                    using (StreamReader reader = new StreamReader(responseStream))
                    {

                        responseCloudDto.FileCSVDownloadedDto.Content = reader.ReadToEnd(); // Now you process the response body.
                    }
                }
                responseCloudDto.StatusCode = (int)HttpStatusCode.OK;


            }

            catch (AmazonS3Exception s3Exception)
            {
                responseCloudDto.StatusDownloadFile = StatusDownloadFile.Error;
                responseCloudDto.Message = "error";
                _logger.LogError(string.Format("Error in download S3 file: PSafeId:'{0}', Product:'{1}', Exception:'{2}'", psafeID, productName, s3Exception.Message));
            }
            return responseCloudDto;
        }

    }
}
