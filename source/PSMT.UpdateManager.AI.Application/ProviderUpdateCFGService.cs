﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using PSMT.UpdateManager.AI.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using PSMT.UpdateManager.AI.Contracts.Cache;
using PSMT.UpdateManager.AI.Application.CryptoHelper;
using System.Text.Json;
using System.IO;

namespace PSMT.UpdateManager.AI.Application
{
    public class ProviderUpdateCFGService : IProviderUpdateCFGService
    {
        private const int LAST_RANGE_VERSION = 999999;


        #region attributes
        private readonly ICloudService _cloudService;
        private readonly ILogger<ProviderUpdateCFGService> _logger;
        private readonly CloudServiceSettings _cloudServiceSettings;
        private readonly ICacheService _cacheService;

        #endregion


        #region private methods
        private UpdateCFG GenerateUpdateCFG(UpdateCFG updateCFG, PSafeUserData user, int sourceId, string productPath)
        {
            IList<PSafeUserFile> newFileList = user.PsafeUserFiles.Where(f => f.SourceId == sourceId).ToList();
            if (newFileList.Count() > 0)//removing all files from notification manager
            {

                if (sourceId == 1)
                    updateCFG.files.RemoveAll(f => f.file.Contains("notification_l"));
                else
                if (sourceId == 2)
                    updateCFG.files.RemoveAll(f => f.file.Contains("cardlist_l"));
                else
                if (sourceId == 3)
                    updateCFG.files.RemoveAll(f => f.file.Contains("dialog_l"));

                AI.Contracts.File file = null;

                int fileId = 1;
                foreach (PSafeUserFile newFile in newFileList)
                {
                    file = new AI.Contracts.File();
                    file.id = fileId;
                    file.file = newFile.Url.Split('/').Last();
                    file.serverFile = newFile.Url;
                    file.timestamp = user.Timestamp;
                    file.hash = newFile.Hash;
                    file.filter = new Filter();
                    file.filter.versionCodeRange = new VersionCodeRange();
                    file.filter.versionCodeRange.first = user.Version;
                    file.filter.versionCodeRange.last = LAST_RANGE_VERSION;
                    file.uploadType = "IA";

                    updateCFG.files.Add(file);
                    fileId++;
                }
            }

            return updateCFG;
        }

        private byte[] ConvertToByteArray(string fileContent)
        {
            byte[] fileCreated;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                TextWriter tw = new StreamWriter(memoryStream);

                tw.WriteLine(fileContent);
                tw.Flush();

                fileCreated = memoryStream.ToArray();
            }


            return fileCreated;
        }

        private UpdateCFG MergeFiles(string psafeId, string userData, UpdateCFG updateCFGFromCache, string productPath)
        {
            UpdateCFG updateCFG = null;

            try
            {

                PSafeUserData user = new PSafeUserData(userData);
                if (user.IsInvalid(psafeId))
                {
                    throw new ApplicationException($"Invalid User data in .csv file. PSAfeId '{psafeId}'");
                };

                //notification files first(SourceType =1)
                updateCFGFromCache = this.GenerateUpdateCFG(updateCFGFromCache, user, 1, productPath);//notification
                updateCFGFromCache = this.GenerateUpdateCFG(updateCFGFromCache, user, 2, productPath);//cardlist
                updateCFGFromCache = this.GenerateUpdateCFG(updateCFGFromCache, user, 3, productPath);//dialog

                updateCFG = updateCFGFromCache;

                // updateCFGFromCache.files


            }
            catch (Exception e)
            {
                throw new ApplicationException($"Invalid User data in .csv file. PSAfeId '{psafeId}'. Eception:'{e.Message}'");
            }

            return updateCFG;
        }

        #endregion


        #region constructor
        public ProviderUpdateCFGService(ICloudService cloudService, ILogger<ProviderUpdateCFGService> logger,
            IOptions<CloudServiceSettings> cloudServiceSettings, ICacheService cacheService)
        {
            _cloudService = cloudService;
            _logger = logger;
            _cloudServiceSettings = cloudServiceSettings.Value;
            _cacheService = cacheService;

        }

        #endregion

        #region public methods
        public async Task<UpdateCFG> GetUpdateCFGFileAsync(int productId, string psafeId)
        {
            if (string.IsNullOrEmpty(psafeId))
                throw new ApplicationException("error");

            Domain.Product product = new Domain.Product(productId);

            CachedData cachedData = await _cacheService.GetDataAsync(product.KeyToCache);

            if (cachedData == null)
            {
                _logger.LogError($"No data from cache key:'{product.KeyToCache}'");
                throw new ApplicationException("cache_error");
            }
            else//panic button for that producto On, so the API does not delivers nothing....
             if (cachedData.IsPanicOn)
            {
                _logger.LogWarning($"Panic on for product :'{product.KeyToCache}'");
                throw new ApplicationException("cache_error");
            }
            if (cachedData.CachedConfiguration.Product.IsActive)
            {

            
                string urlToDowloadUserConfigFile = string.Format(_cloudServiceSettings.urlToDowloadUserConfigFile, product.Path, psafeId);


                ResponseCloudDto responseCloudDto = await _cloudService.DownloadUserCSVFileByHttpRequestAsync(urlToDowloadUserConfigFile, product.Id, product.Name, psafeId);

                if (responseCloudDto.StatusDownloadFile == StatusDownloadFile.Success)
                {

                    //get from cache
                    UpdateCFG updateCFG = cachedData.UpdateCFG;

                    //merge the cache one with the user one...
                    updateCFG = this.MergeFiles(psafeId, responseCloudDto.FileCSVDownloadedDto.Content, updateCFG, product.Path);

                    _logger.LogInformation($"Update.cfg created successfull.PSafeId:'{psafeId}'");
                    return updateCFG;
                }
                else
                {
                    _logger.LogError(responseCloudDto.Message);
                    throw new ApplicationException("error");
                }

            }
            else
            {
                _logger.LogError($"Product '{product.Name}' not active to get updateCFG file from IA!");
                throw new ApplicationException("error");
            }
        }


        public async Task<byte[]> GetUpdateCFGFileEncryptedAsync(int productId, string psafeId)
        {
            UpdateCFG updateCFG = await GetUpdateCFGFileAsync(productId, psafeId);


            RSACryptoHelper rsaCryptoHelper = new RSACryptoHelper();
            byte[] updateCFGFileEncryptedAsByte = rsaCryptoHelper.SignFile(this.ConvertToByteArray(JsonSerializer.Serialize(updateCFG)));


            return updateCFGFileEncryptedAsByte;

        }

        public async Task<bool> ActivatePanicButton(int productId, bool active)
        {
            try
            {
                Domain.Product product = new Domain.Product(productId);

                CachedData cachedData = await _cacheService.GetDataAsync(product.KeyToCache);

                if (cachedData == null)
                {
                    cachedData = new CachedData(product.Id, product.Name, false, null);
                    cachedData.IsPanicOn = active;
                }
                else
                    cachedData.IsPanicOn = active;

                await _cacheService.AddDataAsync(product.KeyToCache, JsonSerializer.Serialize(cachedData));
                _logger.LogInformation($"Panic action '{active}' in product '{productId}' successfull!");
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in Panic action '{active}' in product '{productId}'. Error:'{e.Message}'");
                throw new ApplicationException($"Error in Panic action '{active}' in product '{productId}'. Error:'{e.Message}'");

            }

        }


        public async Task<bool> GetPanicButton(int productId)
        {
            //Set in 05/05/2022 because Redis wa disabled
            return true;

            try
            {
                Domain.Product product = new Domain.Product(productId);

                CachedData cachedData = await _cacheService.GetDataAsync(product.KeyToCache);

                if (cachedData == null)
                {
                    return false;
                }
                else
                    return cachedData.IsPanicOn;
            }
            catch (Exception e)
            {
                _logger.LogError($"Error in Panic button reading in product '{productId}'. Error:'{e.Message}'");
                throw new ApplicationException($"Error in Panic button reading in product '{productId}'. Error:'{e.Message}'");

            }

        }

        #endregion
    }
}
