﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cloud;
using PSMT.UpdateManager.AI.Contracts.Queue;
using PSMT.UpdateManager.AI.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Application
{
    public class CheckFileService: ICheckFileService
    {
        private readonly ILogger<CheckFileService> _logger;
        private readonly ICloudService _cloudService;
        private readonly IQueueService _queueService;
        private readonly CloudServiceSettings _cloudServiceSettings;
        


        #region private methods
        private string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }


        private long CalculateTimeStamp()
        {
            var timeSpan = (System.DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0));

            return (long)timeSpan.TotalSeconds;
        }


        private IDictionary<int, IList<PSafeUserData>> GetPSafeUserDataSplitted(string filename, long timestamp)
        {
            int totalFileLines = System.IO.File.ReadLines(filename).Count();
            int size = 100000;
            int blockSize = (totalFileLines / size);

            IEnumerable<string> psafeList = new string[] { };
            _logger.LogInformation(string.Format("Start read '{0}'", DateTime.Now));

            /*FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
            using (StreamReader reader = new StreamReader(fileStream))
            {
                while (!reader.EndOfStream)
                {
                    psafeList = psafeList.Append(reader.ReadLine());
                   
                }
            }*/

            //must verify if this file ha ended the hole process...if so, return....
            int currentBlockSize = 1;
            int currenLine = 1;
            PSafeUserData user;
            IList<PSafeUserData> users = new List<PSafeUserData>();
            //FileInfo fi = new FileInfo(filename);
            using (StreamReader sr = System.IO.File.OpenText(filename))
            {
                
                string s = String.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    if(currenLine == (size * currentBlockSize))//each 100.000 lines changes the block
                    {
                        currentBlockSize++;
                        //save data in DataBase....
                    }

                    user = new PSafeUserData(s);
                    users.Add(user);


                    //psafeList = psafeList.Append(s);
                    currenLine++;
                }
            }

            _logger.LogInformation(string.Format("End  read '{0}'", DateTime.Now));
       
            IDictionary<int, IList<PSafeUserData>> userDataSplitted = new Dictionary<int, IList<PSafeUserData>>();

            // string[] lines = psafeList.Count();
            
            //IList<PSafeUserData> users = new List<PSafeUserData>();

            
           // PSafeUserData user;
            if (totalFileLines <= size)
            {
                foreach (string line in psafeList)
                {
                    user = new PSafeUserData(line);
                    if (user.IsInvalid(""))
                        continue;
                    users.Add(user);
                }
                userDataSplitted.Add(1, users);
            }
            else
            {
                IEnumerable<string> blockedlines;
                for (int i = 1; i <= blockSize; i++)
                {
                    users = new List<PSafeUserData>();
                    blockedlines = psafeList.Take(size).ToArray();
                    foreach (string line in blockedlines)
                    {
                        user = new PSafeUserData(line);
                        if (user.IsInvalid(""))
                            continue;
                        users.Add(user);
                    }
                    userDataSplitted.Add(i, users);
                    psafeList = psafeList.Skip(size);
                }

            }

            return userDataSplitted;

        }



            private void NextStep(ResponseCloudDto responseCloudDto)
        {
            if (responseCloudDto.StatusDownloadFile != StatusDownloadFile.Error)
            {
                //responseCloudDto.FileCSVDownloadedDto.MD5 = this.CreateMD5(responseCloudDto.FileCSVDownloadedDto.Content);
                //_logger.LogInformation(string.Format("File from Product '{0}' downloaded OK. MD5 '{1}'", responseCloudDto.ProductName, responseCloudDto.FileCSVDownloadedDto.MD5));

                _logger.LogInformation(string.Format("Starting enqueue Product '{0}' at: {1}", responseCloudDto.ProductName, DateTimeOffset.Now));

                //this.GetContentFile(responseCloudDto.FileName);
                //_queueService.PublishMessage(responseCloudDto.ProductName + "_queue", responseCloudDto.FileCSVDownloadedDto.Content);

                var t = this.GetPSafeUserDataSplitted(responseCloudDto.FileName, this.CalculateTimeStamp());

                _logger.LogInformation(string.Format("Finished enqueue Product '{0}' at: {1}", responseCloudDto.ProductName, DateTimeOffset.Now));

                _logger.LogInformation(string.Format("Finished process for product {0} at: {1}", responseCloudDto.ProductName, DateTimeOffset.Now));

            }
            else
                _logger.LogError(string.Format("File from Product '{0}' downloaded ERROR: '{1}'", responseCloudDto.ProductName, responseCloudDto.Message));
        }

        private void GetContentFile(string filename)
        {
            var att = System.IO.File.ReadLines(filename).Count();
            int i = 1;
            FileStream fileStream = new FileStream(filename, FileMode.Open);
            //fileStream.
            using (StreamReader reader = new StreamReader(fileStream))
            {
              
                while (!reader.EndOfStream)
                {
                    Console.WriteLine("Numero " + i + reader.ReadLine());
                    i++;
                }
            }
        }

            #endregion

            public CheckFileService(ICloudService cloudService, ILogger<CheckFileService> logger, 
            IOptions<CloudServiceSettings> cloudServiceSettings, IQueueService queueService)
        {
            _cloudService = cloudService;
            _cloudServiceSettings = cloudServiceSettings.Value;
            _logger = logger;
            _queueService = queueService;

            //_taskList = new List<Task>();
        }


        public async Task ExecuteAsync(int productId)
        {
            Product product = new Product(productId);

            string urlToDlowload = string.Format(_cloudServiceSettings.urlToDownloadCSVFile, product.Path, product.Path);

            ResponseCloudDto t = await _cloudService.DownloadCompleteCSVFileAsync(urlToDlowload, product.Id, product.Name);

            this.NextStep(t);

            //Task<ResponseCloudDto> task =  await _cloudService.DownloadFile(urlToDlowload, productId, productName);

            //return task.ContinueWith(c => this.NextStep(c.Result));
            //Task nextStep2 = task.ContinueWith(c => this.NextStep(c.Result));
            //nextStep2. ContinueWith(c => this.NextStep2(c.Start Result.));
            //return nextStep2;
           
        }

       

    }
}
