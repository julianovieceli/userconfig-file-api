﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts;
using PSMT.UpdateManager.AI.Contracts.Cache;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Application
{
    public class CacheableUpdateCFGService: ICacheableUpdateCFGService
    {

        #region attributes
        private readonly ILogger<CacheableUpdateCFGService> _logger;
        private readonly CacheableServiceConfiguration _cacheableServiceConfiguration;
        private readonly ICacheService _cacheService;


        private HttpClient _client;

        #endregion

        #region constructor

        public CacheableUpdateCFGService(IOptions<CacheableServiceConfiguration> cacheableServiceConfiguration, ILogger<CacheableUpdateCFGService> logger,
            ICacheService cacheService)
        {
            _logger = logger;
            _cacheService = cacheService;
            _cacheableServiceConfiguration = cacheableServiceConfiguration.Value;
        }

        #endregion

        #region private methods

        private string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }

        private bool CheckIfEqual(UpdateCFG updateCFGfromSource, CachedData cachedData)
        {
            //if ther isnt in cache, suer are differents...
            if ((cachedData == null) || (cachedData.UpdateCFG == null))
                return false;

            string md5FromSource = this.CreateMD5(JsonSerializer.Serialize(updateCFGfromSource));

            string md5FromCache = this.CreateMD5(JsonSerializer.Serialize(cachedData.UpdateCFG));

            return (md5FromSource == md5FromCache);
        }

        private async Task<UpdateCFG> DownloadUpdateCFG(long productId)
        {

            UpdateCFG updateCFG = null;
            string urlToDownload = string.Format(_cacheableServiceConfiguration.urlToGetLasUpdateCFGfile, productId);

            try
            {
                      //http://updatemanager.psafe.net/client/api/DownloadUpdateCFGFile?productId={0}
                HttpResponseMessage response = await _client.GetAsync(urlToDownload);
                if (response.IsSuccessStatusCode)
                {
                    using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync())
                    {
                        using (StreamReader sr = new StreamReader(streamToReadFrom))
                        {
                            var result = sr.ReadLine();
                            updateCFG = JsonSerializer.Deserialize<UpdateCFG>(result);

                        }
                    }

                }
                else
                {
                    string msgError = $"Error in add cache data for product:'{productId}', url:'{urlToDownload}', error:'{response.StatusCode}'";
                   // _logger.LogError(msgError);
                    throw new ApplicationException(msgError);
                }
                
            }
            catch (Exception e)
            {
                string msgError = $"Error in add cache data for product:'{productId}', url:'{urlToDownload}', error:'{e.Message}'";
               // _logger.LogError(msgError);
                throw new ApplicationException(msgError);
            }

            return updateCFG;
        }


        #endregion


        #region public methods

        public async Task UploadUpdateCFGToCache()
        {
            UpdateCFG updateCFGFromSource = null;
            CachedData cachedData = null;
            Domain.Product product = null;

            string dataError = string.Empty;

            foreach (var p in _cacheableServiceConfiguration.productListToAddToCache)
            {
                product = new Domain.Product(Convert.ToInt32(p.Key));

                dataError = $"ProductId':{product.Id}', Product Name'{product.Name}', Product path:'{product.Path}'";

                cachedData = await _cacheService.GetDataAsync(product.KeyToCache);
                //If panic butto on for that product, doesnothing...
                if ((cachedData != null) && (cachedData.IsPanicOn))
                {
                    _logger.LogInformation($"Panic button for product '{product.Name}'On. No data is added even deleted from cache. Api will ignore.");
                    continue;
                }

                if (p.Value)//must add or update cache
                {
                    try
                    {
                        using (_client = new HttpClient())
                        {

                            updateCFGFromSource = await this.DownloadUpdateCFG(product.Id);


                            //if the file from source(download form updatemanager egacy endpoint)
                            //is diff from the one that is in cache...and is active to add to cache, so adds...
                            if (!this.CheckIfEqual(updateCFGFromSource, cachedData))
                            {
                                cachedData = new CachedData(product.Id, product.Name, true, updateCFGFromSource);
                                await _cacheService.AddDataAsync(product.KeyToCache, JsonSerializer.Serialize(cachedData));
                                _logger.LogInformation($"Cache for product '{product.Name}' added successfull");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"{dataError}. - Error trying to get file from source and add to cache. Error:'{e.Message}'");

                    }
                }
                else//must remove from cache...
                {
                    try
                    {
                        cachedData = new CachedData(product.Id, product.Name, false, null);
                        await _cacheService.AddDataAsync(product.KeyToCache, JsonSerializer.Serialize(cachedData));
                        _logger.LogInformation($"Cache for product '{product.Name}' disabled successfull");
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"{dataError}. -Error trying to remove data from cache. Error:'{e.Message}'");
                    }


                }

            }
        }

        #endregion
    }
}
