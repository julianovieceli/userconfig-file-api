﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace PSMT.UpdateManager.AI.Application.CryptoHelper
{
    public class RSACryptoHelper
    {
        private const string resourceName = "PSMT.UpdateManager.AI.Application.CryptoHelper.private.pem";

        #region private methods

        /// <summary>
        /// Encripts the file as AES encription and PKCS7
        /// </summary>
        /// <param name="plainTextByte"></param>
        /// <returns></returns>
        private byte[] EncryptAES(byte[] file)
        {
            sbyte[] key = { 18, 31, -124, 33, -91, 35, 28, 66, 4, -41, 1, 21, -25, 35, -8, 14 };
            byte[] keySigned = (byte[])(Array)key;
            sbyte[] iv = { -81, -83, 24, -92, 18, 114, -44, 7, -22, -40, -96, 23, -8, -123, 31, -124 };
            byte[] ivSigned = (byte[])(Array)iv;

            using (RijndaelManaged myRijndael = new RijndaelManaged())
            {
                myRijndael.BlockSize = 128;
                myRijndael.KeySize = 256;
                myRijndael.Mode = CipherMode.CBC;
                myRijndael.Padding = PaddingMode.PKCS7;


                myRijndael.Key = keySigned;
                myRijndael.IV = ivSigned;

                ICryptoTransform encrypto = myRijndael.CreateEncryptor();

                //byte[] plainTextByte = ASCIIEncoding.UTF8.GetBytes(plainText);
                byte[] cipherText = encrypto.TransformFinalBlock(file, 0, file.Length);
                //return Convert.ToBase64String(CipherText);
                return cipherText;

            }
        }

        /// <summary>
        /// Gets the private key from pem file and returns a RSACryptoServiceProvider to sign
        /// </summary>
        /// <returns></returns>
        private RSACryptoServiceProvider GetPrivateKeyFromPEMFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            RSAParameters rsaParameters = new RSAParameters();
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                TextReader tr = new StreamReader(stream);
                PemReader pemReader = new PemReader(tr);
                Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters key = (Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters)pemReader.ReadObject();
                rsaParameters = DotNetUtilities.ToRSAParameters(key);
            }

            RSACryptoServiceProvider rsaProvider = new RSACryptoServiceProvider(4096);
            rsaProvider.ImportParameters(rsaParameters);

            return rsaProvider;
        }


        #endregion

        #region public methods

        /// <summary>
        /// Encrypt and Signs the file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public byte[] SignFile(byte[] file)
        {
            RSACryptoServiceProvider rsaProvider = this.GetPrivateKeyFromPEMFile();

            var sha = new SHA1CryptoServiceProvider();

            //Envrypts the file as AES
            byte[] fileEncripted = this.EncryptAES(file);

            ///gets the hash of the file encripted
            byte[] hash = sha.ComputeHash(fileEncripted);

            ///signs the encripted hash file with SHA1
            byte[] signature = rsaProvider.SignHash(hash, CryptoConfig.MapNameToOID("SHA1"));

            int value = signature.Length;

            byte[] assignedFile = null;

            using (MemoryStream ms = new MemoryStream())
            {

                BinaryWriter writeBinay = new BinaryWriter(ms);
                writeBinay.Write(Convert.ToInt32((value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 | (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24));
                writeBinay.Write(signature);
                writeBinay.Write(fileEncripted);
                writeBinay.Close();
                assignedFile = ms.ToArray();
            }

            return assignedFile;
        }

        #endregion


    }
}
