using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PSMT.UpdateManager.AI.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Service
{
    public class AIService : BackgroundService
    {
        private readonly ILogger<AIService> _logger;
        private readonly AIServiceConfiguration _AIServiceConfiguration;
        private readonly ICheckFileService _checkFileService;

       

        public AIService(ILogger<AIService> logger, IOptions<AIServiceConfiguration> AIServiceConfiguration, ICheckFileService checkFileService)
        {
            _logger = logger;

            _AIServiceConfiguration = AIServiceConfiguration.Value;
            _checkFileService = checkFileService;



        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Task[] takList = new Task[this._AIServiceConfiguration.productListToExecute.Length];

            int i = 0;
            while (!stoppingToken.IsCancellationRequested)
            {

                _logger.LogInformation(string.Format("AI Service is starting at: {0}", DateTimeOffset.Now));

                foreach (int productId in this._AIServiceConfiguration.productListToExecute)
                {
                    _logger.LogInformation(string.Format("Starting process for product {0} at: {1}", productId, DateTimeOffset.Now));

                    takList[i] = _checkFileService.ExecuteAsync(productId);
                    i++;
                }

                Task.WaitAll(takList);
                _logger.LogInformation(string.Format("AI Service task is finishing at: {0}", DateTimeOffset.Now));
                i = 0;
                //_checkFileService.ExecuteTasks();



                /*string fileDSECNot = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\example_output.csv";
                string fileDSECRPM = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\example_outputRPM.csv";

                Task[] taskList = new Task[4];
                taskList[0] = this.ReadFile(fileDSECNot);
                taskList[1] = this.ReadFile(fileDSECRPM);
                taskList[2] = this.ReadFile2(fileDSECNot);
                taskList[3] = this.ReadFile2(fileDSECRPM);
                */

                //Task t1 = new Task( delegate { this.ReadFile3(fileDSECRPM); });
                // await t1;

                //  foreach (Task t in taskList)
                // {
                //     await t;
                //  }

                //await taskList[3];


                //await Task.WhenAll(taskList);


                TimeSpan ts = new TimeSpan(0, _AIServiceConfiguration.periodInMinutes, 0);
                await Task.Delay(ts, stoppingToken);


               
            }
        }

        public IEnumerable<string> ReadFile3(string fileName)
        {
             return System.IO.File.ReadLines(fileName);


        }

        public Task ReadFile(string fileName)
        {
            return new Task(() =>
            {
                var t = System.IO.File.ReadLines(fileName);
            });

        }

        public async Task<string[]> ReadFile2(string fileName)
        {
            return await System.IO.File.ReadAllLinesAsync(fileName);

        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            

            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            

           return base.StopAsync(cancellationToken);
        }


        /*public Task ShowMessage(string msg)
        {
            await _logger.LogInformation(msg);
        }*/

    }
}
