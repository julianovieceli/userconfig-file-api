﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Contracts.Cache
{
    public interface ICacheService
    {
        Task<CachedData> GetDataAsync(string key);

        Task AddDataAsync(string key, string data);

        Task RemoveDataAsync(string key);
    }
}
