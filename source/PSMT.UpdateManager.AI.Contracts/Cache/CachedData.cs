﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts.Cache
{
    public class CachedData
    {
        public CachedConfiguration CachedConfiguration { get; set; }
        public UpdateCFG UpdateCFG  { get;set;}


        public bool IsPanicOn { get; set; }

        public CachedData(long productId, string productName, bool isActive, UpdateCFG updateCFG)
        {
            CachedConfiguration = new CachedConfiguration(productId, productName, isActive);
            this.UpdateCFG = updateCFG;
        }

        public CachedData()
        {

        }
    }

    public class CachedConfiguration
    {
        public Product Product { get; set; }


        public CachedConfiguration(long productId, string productName, bool isActive)
        {
            Product = new Product();
            Product.Id = productId;
            Product.Name = productName;
            Product.IsActive = isActive;
        }

        public CachedConfiguration()
        {

        }

    }
}
