﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts.Cache
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
