﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts
{
    public class CacheableServiceConfiguration
    {
        public int periodInMinutes { get; set; }
        public Dictionary<string, bool> productListToAddToCache { get; set; }

        public string urlToGetLasUpdateCFGfile { get; set; }
    }
}
