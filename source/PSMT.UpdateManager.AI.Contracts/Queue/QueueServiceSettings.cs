﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts.Queue
{
    public class QueueServiceSettings
    {
        public string localhost { get; set; }
        public string user { get; set; }
        public string password { get; set; }

        public int port { get; set; }
    }
}
