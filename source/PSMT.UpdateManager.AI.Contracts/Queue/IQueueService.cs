﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts.Queue
{
    public interface IQueueService
    {
        void PublishMessage(string queueName, string message);
    }
}
