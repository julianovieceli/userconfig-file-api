﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Contracts
{
    /// <summary>
    /// Classe that represents the json of the update.cfg
    /// </summary>
    public class UpdateCFG
    {
        public List<File> files { get; set; }
        public UpdateVersion updateVersion { get; set; }
    }

    public class VersionCodeRange
    {
        public int first { get; set; }
        public int last { get; set; }
    }

    public class Filter
    {
        public List<int> versionCodes { get; set; }
        public VersionCodeRange versionCodeRange { get; set; }
    }

    public class File
    {
        public long id { get; set; }
        public string file { get; set; }
        public string serverFile { get; set; }
        public long timestamp { get; set; }
        public string hash { get; set; }
        public Filter filter { get; set; }
        public AbTest abTest { get; set; }
        public string description { get; set; }
        
        public string uploadType { get; set; }

    }

    public class AbTest
    {
        public string testId { get; set; }
        public string variantName { get; set; }
        public int distributionFirst { get; set; }
        public int distributionLast { get; set; }
    }

    public class NotifyOldVersion
    {
        public Filter filter { get; set; }
        public bool force { get; set; }
    }

    public class UpdateVersion
    {
        public int versionCode { get; set; }
        public string version { get; set; }
        public List<NotifyOldVersion> notifyOldVersions { get; set; }
    }
}
