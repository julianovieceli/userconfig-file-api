﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Contracts
{
    public interface ICheckFileService
    {
        Task ExecuteAsync(int productId);
        //void ExecuteTasks();
    }
}
