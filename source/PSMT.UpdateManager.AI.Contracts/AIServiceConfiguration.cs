﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts
{
    public class AIServiceConfiguration
    {
        public int periodInMinutes { get; set; }
        public int[] productListToExecute { get; set; }
    }
}
