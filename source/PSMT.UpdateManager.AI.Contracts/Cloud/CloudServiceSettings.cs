﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts.Cloud
{
    public class CloudServiceSettings
    {
        public string urlToDownloadCSVFile { get; set; }

        public string urlToDowloadUserConfigFile { get; set; }

        public string filesPath { get; set; }
        public string bucketName { get; set; }

        public string key { get; set; }

        public string region { get; set; }

        public string accessKey { get; set; }

        public string secretKey { get; set; }
    }
}
