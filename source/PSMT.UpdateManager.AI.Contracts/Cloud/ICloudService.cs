﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Contracts.Cloud
{
    public interface ICloudService
    {
        Task<ResponseCloudDto> DownloadCompleteCSVFileAsync(string productPath, long productId, string productName);


        Task<ResponseCloudDto> DownloadUserCSVFileByHttpRequestAsync(string userPath, long productId, string productName, string psafeID);


        [Obsolete("Use NewMethod DownloadUserCSVFileByHttpRequestAsync instead")]
        Task<ResponseCloudDto> DownloadUserCSVFileByClientSDKAsync(string userPath, long productId, string productName, string psafeID);
    }
}
