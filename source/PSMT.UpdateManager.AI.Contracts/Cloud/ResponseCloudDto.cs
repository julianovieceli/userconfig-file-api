﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Contracts.Cloud
{
    public enum StatusDownloadFile { Success = 1, Error = 2 , AlreadyExists = 3}
    public class ResponseCloudDto
    {
        public long ProductId { get; set; }

        public string PSafeId { get; set; }

        public string ProductName { get; set; }

        public int StatusCode { get; set; }

        public StatusDownloadFile StatusDownloadFile { get; set; }

        public string Message { get; set; }

        public FileCSVDownloadedDto FileCSVDownloadedDto { get; set; }

        public string FileName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(PSafeId))//this case is by user, the file content is one line....
                    return "user_config.csv";

                return $"output_{ProductName}.csv";
            }
        }

        public ResponseCloudDto(long productId, string productName)
        {
            ProductId = productId;
            ProductName = productName;
            StatusDownloadFile = StatusDownloadFile.Success;
            FileCSVDownloadedDto = new FileCSVDownloadedDto();
        }
        public ResponseCloudDto(long productId, string productName, string psafeId) : this(productId, productName)
        {
            PSafeId = psafeId;
        }
    }
    public class FileCSVDownloadedDto
    {
        public string Content { get; set; }
        public string MD5 { get; set; }
        public string Name { get; set; }

    }
}
