﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Contracts
{
    public interface IProviderUpdateCFGService
    {
        Task<UpdateCFG> GetUpdateCFGFileAsync(int productId, string psafeId);

        Task<byte[]> GetUpdateCFGFileEncryptedAsync(int productId, string psafeId);

        Task<bool> ActivatePanicButton(int productId, bool active);

        Task<bool> GetPanicButton(int productId);
    }
}
