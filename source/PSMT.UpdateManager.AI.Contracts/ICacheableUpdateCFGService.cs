﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSMT.UpdateManager.AI.Contracts
{
    public interface ICacheableUpdateCFGService
    {
        Task UploadUpdateCFGToCache();
    }
}
