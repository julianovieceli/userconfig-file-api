﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Domain
{
    public class PSafeUserFile
    {
        private string hash;
        private string url;
        private int sourceId;//1-Notification, 2- RPM, 3-Dialog

        public string Hash { get => hash; set => hash = value; }
        public string Url { get => url; set => url = value; }
    
        public int SourceId { get => sourceId; set => sourceId = value; }

        public PSafeUserFile(int sourceId)
        {
            this.sourceId = sourceId;
        }
    }
}
