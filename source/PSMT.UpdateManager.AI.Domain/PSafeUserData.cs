﻿using System;
using System.Collections.Generic;

namespace PSMT.UpdateManager.AI.Domain
{
    public class PSafeUserData
    {
        private string id;
        private int version;
        private IList<PSafeUserFile> psafeUserFiles;
        private long timestamp;
       

        public string Id { get => id; set => id = value; }
        public int Version { get => version; set => version = value; }
        public IList<PSafeUserFile> PsafeUserFiles { get => psafeUserFiles; set => psafeUserFiles = value; }
        public long Timestamp { get => timestamp; set => timestamp = value; }

        public bool IsInvalid(string psafeId)
        {
           return (this.Id != psafeId);
        }


        public PSafeUserData(string userLineData)
        {


            this.psafeUserFiles = new List<PSafeUserFile>();

           
            string[] fields = userLineData.Split(",");


            this.Id = fields[0];
            this.Version = Convert.ToInt32(fields[1]);
            this.Timestamp = Convert.ToInt64(fields[20]);

            PSafeUserFile pSafeUserFile;
            if (!string.IsNullOrWhiteSpace(fields[2]))//data from Notification
            {

                pSafeUserFile = new PSafeUserFile(1);

                pSafeUserFile.Url = fields[2];
                pSafeUserFile.Hash = fields[3];
                this.PsafeUserFiles.Add(pSafeUserFile);

                pSafeUserFile = new PSafeUserFile(1);//notification

                pSafeUserFile.Url = fields[4];
                pSafeUserFile.Hash = fields[5];
                this.PsafeUserFiles.Add(pSafeUserFile);

                pSafeUserFile = new PSafeUserFile(1);

                pSafeUserFile.Url = fields[6];
                pSafeUserFile.Hash = fields[7];
                this.PsafeUserFiles.Add(pSafeUserFile);
            }
            if (!string.IsNullOrWhiteSpace(fields[8]))//data from RPM
            {
                pSafeUserFile = new PSafeUserFile(2);//RPM

                pSafeUserFile.Url = fields[8];
                pSafeUserFile.Hash = fields[9];
                this.PsafeUserFiles.Add(pSafeUserFile);

                pSafeUserFile = new PSafeUserFile(2);//RPM

                pSafeUserFile.Url = fields[10];
                pSafeUserFile.Hash = fields[11];
                this.PsafeUserFiles.Add(pSafeUserFile);

                pSafeUserFile = new PSafeUserFile(2);//RPM

                pSafeUserFile.Url = fields[12];
                pSafeUserFile.Hash = fields[13];
                this.PsafeUserFiles.Add(pSafeUserFile);
            }
            if (!string.IsNullOrWhiteSpace(fields[14]))//data from Dialog
            {
                pSafeUserFile = new PSafeUserFile(3);//Dialog

                pSafeUserFile.Url = fields[14];
                pSafeUserFile.Hash = fields[15];
                this.PsafeUserFiles.Add(pSafeUserFile);

                pSafeUserFile = new PSafeUserFile(3);//Dialog

                pSafeUserFile.Url = fields[16];
                pSafeUserFile.Hash = fields[17];
                this.PsafeUserFiles.Add(pSafeUserFile);

                pSafeUserFile = new PSafeUserFile(3);//Dialog

                pSafeUserFile.Url = fields[18];
                pSafeUserFile.Hash = fields[19];
                this.PsafeUserFiles.Add(pSafeUserFile);
            }

        }


        
    }
}
