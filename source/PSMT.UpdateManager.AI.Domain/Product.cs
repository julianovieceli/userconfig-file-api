﻿using PSMT.Common.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSMT.UpdateManager.AI.Domain
{
    public class Product : Entity
    {
        private string path;
        private string name;
        private string keyToCache;


        public string Path { get => path; }
        public string Name { get => name; }

        public string KeyToCache { get => keyToCache; }

        public Product(int id)
        {
            this.Id = id;
            this.InitializeData();

        }

        private void InitializeData()
        {
            switch (this.Id)
            {
                case 1:
                    {
                        this.path = "psafe";
                        this.name = "DFNDR Security";
                        this.keyToCache = this.path;
                        return;
                    }
                case 2:
                    {
                        this.path = "powerpro";
                        this.name = "Power Pro";
                        this.keyToCache = this.path;
                        return;
                    }
                case 3:
                    {
                        this.name = "Stash";
                        this.path = "stash";
                        this.keyToCache = this.path;
                        return;
                    }
                case 4:
                    {
                        this.name = "VPN";
                        this.path = "vpn";
                        this.keyToCache = this.path;
                        return;
                    }
                case 5:
                    {
                        this.name = "DFNDR Cleanup";
                        this.path = "dfndrCleanup";
                        this.keyToCache = this.path;
                        return;
                    }
                case 6:
                    {
                        this.name = "DFNDR Performance V2";
                        this.path = "performanceV2";
                        this.keyToCache = this.path;
                        return;
                    }
                case 7:
                    {
                        this.name = "VPN IOS";
                        this.path = "vpn_ios";
                        this.keyToCache = this.path;
                        return;
                    }
                case 8:
                    {
                        this.name = "ADBlock";
                        this.path = "adblock";
                        this.keyToCache = this.path;
                        return;
                    }
                case 11:
                    {
                        this.path = "enterprise";
                        this.name = "DFNDR Enterprise";
                        this.keyToCache = this.path;
                        return;
                    }


                default:
                    {
                        throw new ApplicationException($"Invalid Product Id:{ this.Id }" );
                       
                        //return;
                    }
            }
        }
    }
}
